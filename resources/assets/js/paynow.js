import Vue from 'vue';

// import App component
import Paynow from './components/Paynow/Paynow'

if (process.env.NODE_ENV === 'production') {
    Vue.config.devtools = false;
    Vue.config.debug = false;
    Vue.config.silent = true;
} else {
    Vue.config.devtools = true;
}

const paynow = new Vue({
    el: "#content-app",
    components: {
        Paynow
    },
    created() {
        console.log('Paynow app ready');
    }
});

window.paynow = paynow;