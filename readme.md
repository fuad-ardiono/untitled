Read me, please.  
  
How to run:  
1. run composer install  
   if have a trouble, delete composer.lock and delete vendor folder, try composer install again    
2. Set up your env file. run cp env.example .env    
3. run php artisan key:generate  
4. Serve this project, php artisan serve  
  
No db required.  
  
Code with ❤ by Muhammad Fuad Ardiono  
2019