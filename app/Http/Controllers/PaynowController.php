<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaynowController extends Controller
{
    public function paynow()
    {
        return view('paynow');
    }
}
